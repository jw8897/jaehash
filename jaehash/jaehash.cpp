//
//  jaehash.cpp
//  jaehash
//
//  Created by Jae Weller on 2/19/19.
//  Copyright © 2019 Jae Weller. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <sstream>  // defines the type std::ostringstream
#include <iomanip>  // defines the manipulator setw

#include "jaehash.hpp"

using namespace std;

void jaehash()
{
    string str;
	
	while( getline(cin, str)) {
		unsigned int total = 0;
		
		for ( auto i=0; i < str.length() ; i++) {
			total *= 7;
			total += str[i];
		}
		
		cout.width(10);
    	cout << std::right << total << " ";
		cout << str;
    	cout << endl;
	}
}


//ignore what's below that's when I misunderstood the directions
void jaehash1 (string input)
{
    int lineLength = 0;
    int tracker = 0;//keeps trak of where line begins
    ostringstream oss;
    
    for(int i = 0; i < input.length(); i++)
    {
        if(isalpha(input[i]))
        {
            lineLength++;
        }
        if(input[i] == '\n') //when the end of a line is reached
        {
            for(int j = 0; j < 9; j++)//make the lineLength right aligned
            {
                oss<<" ";
            }
            
            oss<<lineLength<<" ";//put in the lineLength after inserting the spaces
            
            for(int j = tracker; j<lineLength; j++) //actually put the contents of the line
            {
                oss<<input[j];
            }
            
            oss<<endl; //add the end of the line
            
            tracker = tracker + lineLength;
            lineLength = 0; //we're going to start looking at a new line, so reset lineLngth
        }
        else if(i == input.length()-1)
        {
            for(int j = 0; j < 9; j++)//make the lineLength right aligned
            {
                oss<<" ";
            }
            
            oss<<lineLength<<" ";//put in the lineLength after inserting the spaces
            
            for(int j = tracker; j<lineLength; j++) //actually put the contents of the line
            {
                oss<<input[j];
            }
        }
    }
    
    string output = oss.str();
    for(int i = 0; i<output.length(); i++)
    {
        cout<<output[i];
    }
}
